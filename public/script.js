var PRICE = 9.99;
var LOAD_NUM = 4;

new Vue({
  el: '#app',
  data: {
    initial: true,
    search: 'zoro',
    total: 0,
    items: [],
    results: [],
    cart: [],
    wow: '',
    loading: '',
    lastSearch: '',
    found: true,
    show: false,
    prev_loaded: 0
  },
  filters: {
    uppercase: function (value) {
      return value.toUpperCase() || "";
    },
    currency: function (value) {
      return '$'.concat(value.toFixed(2));
    }
  },
  methods: {

    onSubmit: function () {
      this.loading = "Searching ...";
      this.found = true;
      this.items = [];

      this.$http.get('/search/'.concat(this.search))
        .then(function (res) {
          this.results = res.data;
          this.loada();
        })
        .then(function () {
          for (var i in this.items) {
            switch (true) {
              case (this.items[i].width < 1000):
                this.items[i].price = 7.99;
                break;
              case (this.items[i].width < 2000):
                this.items[i].price = 10.99;
                break;
              default:
                this.items[i].price = 12.99;
            }
          }
        })
        .catch(this.found = false)
        .then(function () {
          this.loading = '';
          this.initial = false;
        });

      this.lastSearch = this.search;
    },

    addItem: function (index) {
      var item = this.items[index];
      var upd = false;
      for (var i in this.cart) {
        if (this.cart[i].id == item.id) {
          this.increment(this.cart[i]);
          upd = true;
          break;
        }
      }
      if (!upd) {
        this.cart.push({
          id: item.id,
          title: item.title,
          price: item.price,
          qty: 1
        });
        this.updatePrice(item.price);
      }
      this.wowCheck();
    },

    loada: function () {
      if (this.items.length < this.results.length) {
        var append = this.results.slice(this.items.length, this.items.length + LOAD_NUM);
        this.items = this.items.concat(append);
      }
    },

    increment: function (item) {
      this.updatePrice(item.price);
      item.qty++;
      this.wowCheck();
    },

    decrement: function (item) {
      this.updatePrice(-item.price);
      if (item.qty > 1) {
        item.qty--;
      } else {
        this.removeItem(this.cart.indexOf(item));
      }
    },

    remove: function (item) {
      this.updatePrice(-item.price * item.qty);
      item.qty = 0;
      var index = this.cart.indexOf(item);
      this.removeItem(index);
    },

    removeItem: function (index) {
      this.cart.splice(index, 1);
    },

    updatePrice: function (price) {
      this.total += price;
    },

    wowCheck: function () {
      if (this.total >= 30 && this.total < 50) {
        this.wow = 'You are rich!';
      } else
        this.wow = '';
    }
  },
  mounted: function () {
    this.onSubmit();

    var vueInstance = this;
    var elem = document.getElementById('product-list-bottom');
    var watcher = scrollMonitor.create(elem);
    watcher.enterViewport(function () {
      vueInstance.loading = "Searching ...";
      vueInstance.loada();
      vueInstance.loading = '';
    });
  },
  computed: {
    noMoreItems: function () {
      return this.items.length == this.results.length && this.results.length > 0;
    }
  }
});

